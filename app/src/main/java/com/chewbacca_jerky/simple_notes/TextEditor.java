/*
    Purpose: Handles Save, Read, and Remove functions.
    Reference: http://www.tutorialspoint.com/android/android_text_to_speech.htm
 */
package com.chewbacca_jerky.simple_notes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.speech.tts.TextToSpeech;
import java.util.Locale;

public class TextEditor extends AppCompatActivity {
    private Button btnSave;
    private Button btnRead;
    private EditText textEdit;
    private TextToSpeech TTS;
    private static String text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_editor);

        //get ids
        btnSave = (Button)findViewById(R.id.btnSave);
        btnRead = (Button)findViewById(R.id.btnRead);
        textEdit = (EditText)findViewById(R.id.textEdit);

        //Reads Text
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text = textEdit.getText().toString();
                readText();
            }
        });

        //Saves to DataBase
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void readText()
    {
        TTS = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener(){
            @Override
            public void onInit(int status)
            {
                if(status != TextToSpeech.ERROR)
                {
                    TTS.setLanguage(Locale.US);
                    TTS.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                }
            }
        });
    }
}