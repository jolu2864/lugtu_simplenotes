package com.chewbacca_jerky.simple_notes;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import java.lang.String;
import java.sql.SQLException;
import java.util.ArrayList;

public class NoteListView extends AppCompatActivity {
    //initialized variables
    private ArrayList<String> listItems = new ArrayList<>();
    private ListView LV;
    private Button btnAdd;
    private ArrayAdapter<String> adapter;
    private PersistenceManager PM;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list_view);
        LV = (ListView)findViewById(R.id.list);
        btnAdd = (Button)findViewById(R.id.btnAdd);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,listItems);
        LV.setAdapter(adapter);

        //Handles if Add Button is clicked. Adds New Entry
        btnAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v){
                addEntry(adapter);
            }
        });

        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = LV.getItemAtPosition(position);
                String temp = (String)o;
                //Toast.makeText(getApplicationContext(),temp,Toast.LENGTH_LONG).show();
                Intent in = new Intent(NoteListView.this, TextEditor.class);
                startActivity(in);
            }
        });
    }

    /*
        Purpose: Creates New Entry
     */
    private void addEntry(ArrayAdapter<String> adapter)
    {
        listItems.add("New Entry");
        adapter.notifyDataSetChanged();
    }

    private void loadList()
    {
        PM = new PersistenceManager(this);

        try {
            PM.open();
        }catch(SQLException e)
        {
            e.printStackTrace();
        }

        PM.insertComment("Entry", "Hello!!!!");


    }
}
