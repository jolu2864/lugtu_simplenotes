package com.chewbacca_jerky.simple_notes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class WelcomeScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        RelativeLayout layout = (RelativeLayout)findViewById(R.id.WelcomeLayout);
        layout.setOnClickListener(new RelativeLayout.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent in = new Intent(WelcomeScreen.this, NoteListView.class);
                startActivity(in);
            }
        });
    }
}
